#!/bin/bash
 
# exit on error
set -e
 
# Setup proxies
proxy=http://proxy.am3.resldap.eu:3128/
export http_proxy=$proxy
export ftp_proxy=$proxy
export https_proxy=$proxy

# create our directories
mkdir -p $HOME/local $HOME/tmux_tmp
cd $HOME/tmux_tmp
 
# download source files for tmux, libevent, and ncurses
wget -O tmux-1.9a.tar.gz https://github.com/tmux/tmux/releases/download/1.9a/tmux-1.9a.tar.gz
wget https://github.com/downloads/libevent/libevent/libevent-2.0.21-stable.tar.gz
wget ftp://ftp.gnu.org/gnu/ncurses/ncurses-6.0.tar.gz
 
# extract files, configure, and compile
 
############
# libevent #
############
tar xvzf libevent-2.0.21-stable.tar.gz
cd libevent-2.0.21-stable
./configure --prefix=$HOME/local --disable-shared
make
make install
cd ..
 
############
# ncurses #
############
tar xvzf ncurses-6.0.tar.gz
cd ncurses-6.0
./configure --prefix=$HOME/local
make
make install
cd ..
 
############
# tmux #
############
tar xvzf tmux-1.9a.tar.gz
cd tmux-1.9a
./configure CFLAGS="-I$HOME/local/include -I$HOME/local/include/ncurses" LDFLAGS="-L$HOME/local/lib -L$HOME/local/include/ncurses -L$HOME/local/include"
CPPFLAGS="-I$HOME/local/include -I$HOME/local/include/ncurses" LDFLAGS="-static -L$HOME/local/include -L$HOME/local/include/ncurses -L$HOME/local/lib" make
cp tmux $HOME/local/bin
cd ..
 
# cleanup
rm -rf $HOME/tmux_tmp
 
echo "$HOME/local/bin/tmux is now available. You can optionally add $HOME/local/bin to your PATH:"
echo 'echo export PATH=$PATH:~/local/bin >~/.bashrc' 
