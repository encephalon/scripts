#!/bin/bash


if [ -z $@ ]
then
function gen_launcher()
{
  echo -e '<span foreground="orange">\xef\x89\xa9 Firefox</span>
  <span foreground="grey">\xef\x87\x90 Rebel Alliance</span>
  <span foreground="red">\xef\x87\x91 Empire</span>
  <span foreground="grey">\xef\x82\x85 Gears</span>
  <span foreground="#40e0d0">\xef\x87\x8e Synergy</span>
  <span foreground="grey">\xef\x84\xa0 xterm</span>
  <span foreground="green">\xef\x86\xbc Spotify</span>
  <span foreground="gold">\xef\x89\xa8 Chrome</span>
  <span foreground="pink">\xef\x86\x98 Slack</span>'
}


echo empty; gen_launcher
else
    APP=$@

    if [ x"empty" = x"${APP}" ]
    then
        $0
#    elif [ -n "/usr/bin/${APP,,}" ]
    else
#    then
#        /usr/sbin/${APP,,} &
    ${APP}
    fi
fi
