#!/bin/bash -x

configfiles=~/.config/executor/*.exec

if [ -z "$@" ]
then
function gen_launcher()
{
  for i in ${configfiles}; do
    source ${i}
    echo -e "<span foreground=\"$Color\">${Glyph} ${Name}</span>"
  done
}

gen_launcher

else
  # strip out markup and determine executable name
  Name=$(echo $@ | sed -e 's/^[^\>]*\>[^A-Za-z]*\ \([a-zA-Z]*\).*/\1/')
  Launch=$( awk -F'=' '/Exec/{print $2}' $(grep -l ${Name} ${configfiles} ) )
  Params=$( awk -F'=' '/Params/{print $2}' $(grep -l ${Name} ${configfiles} ) )
  if [ x"" = x"${Name}" ]
  then
    $0
  elif [ ${Name} = "xclock" ]
  then
    xclock $Params >/dev/null &
  else
    $Launch >/dev/null &
  fi
fi
