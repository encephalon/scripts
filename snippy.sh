#!/bin/bash
# video demo at: http://www.youtube.com/watch?v=90xoathBYfk

# written by "mhwombat": https://bbs.archlinux.org/viewtopic.php?id=71938&p=2
# Based on "snippy" by "sessy" 
# (https://bbs.archlinux.org/viewtopic.php?id=71938)
#
# You will also need "dmenu", "xsel" and "xdotool". Get them from your linux
# distro in the usual way.
#
# To use:
# 1. Create the directory ~/.snippy
#
# 2. Create a file in that directory for each snippet that you want.
#    The filename will be used as a menu item, so you might want to
#    omit the file extension when you name the file. 
#
#    TIP: If you have a lot of snippets, you can organise them into 
#    subdirectories under ~/.snippy.
#
#    TIP: The contents of the file will be pasted asis, so if you 
#    don't want a newline at the end when the text is pasted, don't
#    put one in the file.
#
# 3. Bind a convenient key combination to this script.
#

#    TIP: For AwesomeWM, add something like this to rc.lua
#      awful.key({ modkey }, "s", function ()
#           awful.util.spawn("/home/mthornba/git/public/gitlab-encephalon/scripts/snippy.sh
#
# 4. If using xterm, make sure 'Select to Clipboard' is true

DIR=${HOME}/.snippy
if [[ $1 = "rofi" ]]; then
  MENU="/usr/bin/rofi -dmenu"
else
  MENU="/usr/bin/dmenu"
fi
DMENU_ARGS="-i -b -p Snippy -fn Ubuntu"

cd ${DIR}

# Use the filenames in the snippy directory as menu entries.
# Get the menu selection from the user.
menugen() {
  if [ ${PWD} = "${HOME}/.snippy" ]; then
    FILE=$(ls -1 | ${MENU} ${DMENU_ARGS})
  else
    FILE=$(echo -e "..\n$(ls -1)" | ${MENU} ${DMENU_ARGS})
  fi
  echo $FILE
  test -e "${FILE}" || exit
}

menugen

# If selection is a directory, cd dir and regen the menu
while [[ -d ${DIR}/${FILE} ]]; do
  DIR=${DIR}/${FILE}
  cd ${DIR}
  menugen
done

if [[ -f ${DIR}/${FILE} ]]; then
  # Put the contents of the selected file into the paste buffer.
  xsel --primary < "${DIR}/${FILE}"
  xsel --clipboard < "${DIR}/${FILE}"
  # Paste into the current application.
#  xdotool key ctrl+v		#gui paste
  xdotool key shift+Insert	#cli
fi
